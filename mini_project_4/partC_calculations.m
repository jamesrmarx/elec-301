VT = 25e-3;

% Q1

VC1 = 1.9;
VB1 = 654e-3;
VE1 = 0;
IC1 = 1.295e-3;
IE1 = -1.305e-3;
IB1 = 10.77e-6;

% Q2 

VC2 = 15;
VB2 = 2.9;
VE2 = 1.235;
IC2 = 2.19e-3;
IE2 = -2.206e-3;
IB2 = 15.39e-6;

% Calculated values 

hfe1 = IC1/IB1;
hfe2 = IC2/IB2;
rpi1 = hfe1*(VT/IC1);
rpi2 = hfe2*(VT/IC2);
gm1  = hfe1/rpi1;
gm2  = hfe2/rpi2;

% Gain caluculation

RB1 = 330e3;
RB2 = 20e3;
RBB = RR(RB1, RB2);
RC  = 10e3;
RE  = 560;
RL  = 10e3;
RS  = 5e3;
Rf  = 1000e3; 

VbVpi = -gm1*( RR( RC, ( rpi2 + ( 1+hfe2 )*RR( RE, RL ) ) ) );
VpiVs = ( RR( RBB, rpi1 ) ) / ( RR( RBB, rpi1 ) + RS ); 
VoVb  = ( RR( RE, RL ) * ( 1 + hfe2 ) ) / ( RR( RE, RL ) * ( 1 + hfe2 ) + rpi2 );

Am = VbVpi * VpiVs * VoVb;

% Feedback

Rin = 2.575e3;
Rout = 63.81;
A = -127; % V/V
B = -1/Rf;

Aprime = A*RS;
Af = Aprime/(1+Aprime*B);
Avpv = Af/RS;

Routp = Rout/(1+Aprime*B);
Rinp=  Rin/(1+Aprime*B);

fl3dB = 2.876/(1+Aprime*B) ;
fh3dB = 91.90e3*(1+Aprime*B) ;

beta = (1/RS)*(1/-119.8 - 1/A)

amountFeedback = (1+Aprime*B);
function RR = RR(r1,r2)
    RR = (r1*r2)/(r1+r2);
end

