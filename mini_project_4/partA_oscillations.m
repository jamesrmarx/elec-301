%% Import data
data = readtable("hope.txt");
t = data.Var1;
vo = data.Var2;

%%
figure
plot(t, vo, 'LineWidth', 2, 'Color', 'black')
% xlim([0.577644, 0.578223])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on

%%
% root locus plot
figure
r = 2*pi*10e3; % radius is frequency
x = 0;
y = 0;
th = 0:pi/100:2*pi;
f = r * exp(j*th) + x+j*y;

x1 = -44424.48;
y1 = 44519.72;

x2 = 94.434;
y2 = 62893.01;

plot(real(f), imag(f), 'LineStyle','--', 'Color', 'black');
line([0,0],[8e4, -8e4], 'LineWidth', 1, 'LineStyle', '-', "Color", 'black')
line([-8e4,8e4],[0, 0], 'LineWidth', 1, 'LineStyle', '-', "Color", 'black')
line([0,x1],[0, y1], 'LineWidth', 1, 'LineStyle', '-', "Color", 'black')
line([0,x1],[0, -y1], 'LineWidth', 1, 'LineStyle', '-', "Color", 'black')
% switch between "x" and "+" marker 
line([x1, x1], [y1, y1], 'Marker', "+", 'Color', 'black', 'MarkerSize', 14, 'LineWidth', 1.5)
line([x1, x1], [-y1, -y1], 'Marker', "+", 'Color', 'black', 'MarkerSize', 14, 'LineWidth', 1.5)
axis([-8e4, 8e4, -8e4, 8e4])
pbaspect([1 1 1])
xlabel("$\sigma$", Interpreter="latex", FontSize=16)
ylabel("j$\omega$", Interpreter="latex", FontSize=16)
grid on