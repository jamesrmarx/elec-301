%% import data
data_1k_noIC = readtable("partB_1k_noIC.txt");
data_1k      = readtable("partB_1k.txt");
data_2k      = readtable("partB_2k.txt");
data_4k      = readtable("partB_4k.txt");
data_500     = readtable("partB_500.txt");
data_250     = readtable("partB_250.txt");

t_1k_noIc = data_1k_noIC.Var1;
t_1k      = data_1k.Var1;
t_2k      = data_2k.Var1;
t_4k      = data_4k.Var1;
t_500     = data_500.Var1;
t_250     = data_250.Var1;

v_1k_noIc = data_1k_noIC.Var2;
v_1k      = data_1k.Var2;
v_2k      = data_2k.Var2;
v_4k      = data_4k.Var2;
v_500     = data_500.Var2;
v_250     = data_250.Var2;

%% Plot data

figure
plot(t_1k, v_1k, 'LineWidth', 2, 'Color', 'black')
xlim([9.6, 9.65])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on

figure
plot(t_2k, v_2k, 'LineWidth', 2, 'Color', 'black')
xlim([14.4, 14.6])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on

figure
plot(t_4k, v_4k, 'LineWidth', 2, 'Color', 'black')
xlim([29, 30])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on

figure
plot(t_500, v_500, 'LineWidth', 2, 'Color', 'black')
xlim([9.395, 9.41])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on

figure
plot(t_250, v_250, 'LineWidth', 2, 'Color', 'black')
xlim([4.438, 4.441])
xlabel("time (s)", FontSize=16, Interpreter="latex")
ylabel("$V_o$ (V)", FontSize=16, Interpreter="latex")
grid on
