\providecommand{\main}{.}
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage[labelfont=bf]{caption}
\usepackage{subcaption}
\usepackage{setspace}
\usepackage{circuitikz}
\usepackage{amsmath}
\usepackage{float}
\usepackage{booktabs}
\usepackage{color, colortbl}

\definecolor{Gray}{gray}{0.9}

%\captionsetup[figure]{font = {stretch=1.35}}


\title{ELEC 301 Mini Project 4 }
\author{James Marx \\ \\ 80562549}
\date{December 9, 2022
\\
\vspace{15cm}
\hspace{8cm} Signature \hrulefill}

\begin{document}

\maketitle
\newpage

\tableofcontents
\listoffigures
\listoftables

\newpage

\section{An Active Filter}

Figure \ref{fig:partA_circuit} shows a second order low pass filter who's transfer function is given by,

\[ H(s) = A_M \frac{1/(RC)^2}{s^2 + s\frac{3-A_M}{RC} + \frac{1}{(RC)^2}} \]

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{figures/partA_circuit.PNG}
    \caption{Filter in Circuit Maker}
    \label{fig:partA_circuit}
\end{figure}


The pass-band gain, $A_M$, is given by $A_M = 1 + R_2/R_1$. We wish to design the circuit such that it is a second order Butterworth filter. This will be done with the requirements that $R = 10\text{k}\Omega$, $R_1 + R_2 = 10\text{k}\Omega$, and a 3dB frequency of $10$kHz. \\

The poles of a second order Butterworth filter are given by the roots of  following polynomial,

\[ s^2 + 2\zeta \omega_o s + \omega_o^2 \]

We can clearly see that the cutoff frequency, $\omega_o$, is given by $1/RC$. Solving for $C$,

\[ C = \frac{1}{R(2\pi f)} = \boxed{1.59nF} \]


To satisfy the requirements of a Butterworth filter, the poles must be evenly spaced in the right half of the $\sigma-j\omega$ plane. For second order, we will have one pair of complex conjugate poles at an angle ($\theta$) of $\pi/2$ from the $\sigma$ axis -- at a distance of $\omega_o$ from the origin. The damping factor, $\zeta$, is given by $\frac{3-A_M}{2}$, thinkig graphicall it can also be given by $\cos(\theta)$. So for a second order Butterworth filter, $\zeta = 1/\sqrt{2}$. Solving for $A_M$ we get,

\[ A_M = 3 - 2\zeta = 3 - \sqrt{2} = \boxed{1.586} \]

We can now solve the following two equations simultaniously,

\[ A_M = 1 + \frac{R_2}{R_1} \]

\[ R_1 + R_2 = 10\text{k}\Omega \]

\[ R_1 = \boxed{6.3\text{k}\Omega} \]

\[ R_2 = \boxed{3.7\text{k}\Omega} \]

The circuit was then built in Circuit Maker as shown in Figure \ref{fig:partA_circuit}. The frequency response of the circuit was then simulated; the magnitude and phase responses are given below in figures \ref{fig:partA_mag} and \ref{fig:partA_phase} respectively.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{figures/partA_mag.png}
    \caption{Filter Magnitude Response}
    \label{fig:partA_mag}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.7\linewidth]{figures/partA_phase.png}
    \caption{Filter Phase Response}
    \label{fig:partA_phase}
\end{figure}

In the magnitude response, we see the expected smooth roll off for $\zeta = 0.707$. The poles for the filter were calculated using the chosen component values and found to be at $s = -44424.48 \pm j44519.72 rad/s$ and are plotted in the $s$-plane in Figure \ref{fig:rlb}. 


\begin{figure}[H]
    \centering
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=1.2\linewidth]{figures/partA_rootlocus1.png}
        \caption{Root Locus as Butterworth Filter}
        \label{fig:rlb}
    \end{subfigure}
    \qquad
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=1.2\linewidth]{figures/partA_rootlocus2.png}
        \caption{Root Locus at Oscillation}
        \label{fig:rlo}
    \end{subfigure}
    \caption{Root Locus Plots}
    \label{fig:rl}
\end{figure}


The values of $R_1$ and $R_2$ were then changed until the circuit began to self-oscillate. Oscillation will first happen when the poles are directly on the $j\omega$ axis. For this to occur $A_M$ $\geq 3$, so $R_2$ must be at least $2$ times larger than $R_1$. While simulating, the circuit began to oscillate at values $R_1 = 3.33\text{k}\Omega$ and $R_2 = 6.67\text{k}\Omega$. This matches expectation, as at these values $A_M$ is $3.003$. Calculating the poles for the new $A_M$ we get $s = 94.434 \pm j62893.01 rad/s$, which is just beginning to push into the left-side of the $s$-plane. These poles are plotted in Figure \ref{fig:rlo}.\\

The output of the circuit while self-oscillating is shown in Figure \ref{fig:oscillation}. The frequency of oscillation is $9.614$kHz.


\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/partA_oscillation_close.png}
    \caption{Filter Output at Self Oscillation}
    \label{fig:oscillation}
\end{figure}

\pagebreak
\section{Phase Shift Oscillator}

The circuit shown in figure \ref{fig:phase_shift} implements a phase shift oscillator. The transfer function for this feed back network is given by,

\begin{figure}[H]
    \centering
    \begin{circuitikz}[american]
        \draw
        % op amp with feed back resistor R_f
        (0, 0) node[op amp] (opamp) {}
        (opamp.-) to[R, l_=$R$] (-3, 0.5)
        (opamp.-) to[short, *-] ++(0, 1.5) coordinate (leftR)
        to[R=$R_f$] (leftR -| opamp.out)
        to[short, -*] (opamp.out)
        (opamp.+) to[short, *-] (-2, -0.5)
        (opamp.out) to[short, -o] ++(1, 0) node[above]{$v_o$}
        (-2, -0.5) to[short] (-2, -1.5)
        (-2, -1.5) to (-2, -1.5) node[ground]{}
        % RC ladder network
        (-3, 0.5) to[C, l_=$C$] (-5, 0.5)
        (-5, 0.5) to[R=$R$] (-5, -1.5)
        (-5, -1.5) to (-5, -1.5) node[ground]{}
        (-5, 0.5) to[C, l_=$C$] (-7, 0.5)
        (-7, 0.5) to[R=$R$] (-7, -1.5)
        (-7, -1.5) to (-7, -1.5) node[ground]{}   
        (-7, 0.5) to[C, l_=$C$] (-9, 0.5)
        (-9, 0.5) to[short] (-9, 4)
        (opamp.out) to[short] ++(0, 4) coordinate (top)
        (-9, 4) to[short] (top)
        ;
    \end{circuitikz}
    \caption{Phase Shift Amplifier}
    \label{fig:phase_shift}
\end{figure}

\[ A_f(j\omega) = \frac{A(j\omega)}{1 + A(j\omega)B(j\omega)} \]

The definition of an oscillator is an amplifier which will have an output for zero input [1]. It can be seen that if the denominator of the transfer function goes to zero, $A_f(j\omega)$ will be infinite and the circuit will oscillate. This will occur when the loop gain $A(j\omega)B(j\omega)$ has unity magnitude and is negative in sign, i.e. a phase shift of $180^{\circ}$; the frequency of oscillation, $\omega_{180}$, will be the frequency for which this occurs. \\

Each pole will contribute a phase shift of $90^{\circ}$, however this is only in the limit. So inorder to have the required phase shift of $180^{\circ}$ occur for finite frequencies, the phase shift oscillator has three capacitors in its RC ladder network. The phase shift oscillator requires the amplifier to have a gain $k$ of $-29$. For a full derivation of this requirement we refer the reader to [2]. The gain of the opamp is set by $-R_f/R$. Therefore to achieve a gain of $-29$ we set $R_f = 29R$. In reality, to ensure oscillations start and are sustained a value of $-29.1$ is selected for all simulations. This will cause the oscillations to grow in magnitude but then be limited by nonlinearities in the circuit forcing the loop gain back to unity. \\

The circuit built in Circuit Maker is shown in Figure \ref{fig:partB_circuit}. This circuit has an initial condition of $5V$ at the output to aid in beginning the oscillations in the circuit. Without the initial condition to took approximately $80-90$ second in simulation time for oscillations to form and become sustained.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{figures/partB_circuit.PNG}
    \caption{Phase Shift Oscillator in Circuit Maker}
    \label{fig:partB_circuit}
\end{figure}

The circuit was the simulated in Circuit Maker, both increasing and decreasing $R$ and $C$ by factors of 2. The value of $R_f$ was adjusted along to keep the relation $R_f/R = 29.1$. The output waveforms of these simulations after stabalizing are shown in Figure \ref{fig:partB_oscillations}.

\begin{figure}[H]
    \centering
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{figures/partB_1k.png}
        \caption{Oscillations for $R = 1\text{k}\Omega$}
        \label{fig:partB_1k}
    \end{subfigure}
    \qquad
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{figures/partB_2k.png}
        \caption{Oscillations for $R = 2\text{k}\Omega$}
        \label{fig:partB_2k}
    \end{subfigure}
    \qquad
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{figures/partB_4k.png}
        \caption{Oscillations for $R = 4\text{k}\Omega$}
        \label{fig:partB_4k}
    \end{subfigure}   
    \qquad
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{figures/partB_500.png}
        \caption{Oscillations for $R = 500\Omega$}
        \label{fig:partB_500}
    \end{subfigure}   
    \qquad
    \begin{subfigure}[t!]{0.4\textwidth}
        \centering
        \includegraphics[width=0.8\linewidth]{figures/partB_250.png}
        \caption{Oscillations for $R = 250\Omega$}
        \label{fig:partB_250}
    \end{subfigure}   
    \caption{Phase Shift Oscillator Output Oscillations}
    \label{fig:partB_oscillations}
\end{figure}

The frequency which the circuit will oscillate at can be calculated from the following equation,

\[ f = \frac{1}{2 \pi \sqrt{6} R C} \]

Using this equation, we can calculate the frequency of oscillation for each set of $R$ and $C$, and compare to the measured values from simulation. These results are summarized in Table \ref{tab:partB_frequencies}.

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g}
            \toprule
            \rowcolor{white}
            $R$ & $C$ & Frequency Measured ($f_m$) & Frequency Calculated ($f_c$) & \% Error \\
            $1\text{k}\Omega$ & $1\mu F$ & $64.89$Hz & $64.97$Hz & $0.12$\% \\
            \rowcolor{white}
            $2\text{k}\Omega$ & $2\mu F$ & $16.23$Hz & $16.24$Hz & $0.06$\% \\
            $4\text{k}\Omega$ & $4\mu F$ & $4.04$Hz & $4.06$Hz & $0.49$\% \\
            \rowcolor{white}
            $500\Omega$ & $0.5\mu F$ & $259.07$Hz & $259.9$Hz & $0.32$\% \\
            $250\Omega$ & $0.25\mu F$ & $1.03$kHz & $1.04$kHz & $0.96$\% \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Frequency of Oscillations from Changing Component Values}
    \label{tab:partB_frequencies}
\end{table}

The frequencies found from simulation agree very well with those calculated, with no measured frequency differing from calculated by more than $1$\%. The parameters were adjusted to ensure a $k$ very close to $-29$, contribuiting to small output distortion, and all measurements were made only after the output became stable, likely contributing to this accuracy.

\pagebreak
\section{Feedback Circuit Experiment}

The circuit shown in Figure \ref{fig:partC_circuit} shows a basic amplifier with a resistive feedback network. This feedback network provides output voltage sampling and input current mixing -- the characteristics of a shunt-shunt topology. To determine the value for $R_{B2}$ to give the largest open-loop gain at $1$kHz, a parameter sweep was performed. After guaging the approximate magnitude of $R_{B2}$, the sweep was performed again, ranging $R_{B2}$ from $19$-$21\text{k}\Omega$. A single half cycle out the output waveform is shown in Figure \ref{fig:param_sweep}, where the bolded trace corresponds to $20\text{k}\Omega$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.5\linewidth]{figures/partC_circuit.PNG}
    \caption{Feedback Circuit}
    \label{fig:partC_circuit}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/partC_param_halfcycle.png}
    \caption{$R_{B2}$ Parameter Sweep Half-cycle}
    \label{fig:param_sweep}
\end{figure}

After determining the appropriate value for $R_{B2}$, the d.c. bias values for each transistor were measured. These values are presented in Table \ref{tab:bias}, with the resulting small signal parameters calculated shown in Table \ref{tab:small_sig}.

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g}
            \toprule
            \rowcolor{white}
            & $V_C$ & $V_B$ & $V_E$ & $I_C$ & $I_E$ & $I_B$ \\
            $Q_1$ & $1.9V$ & $654mV$ & $0V$ & $1.295mA$ & $1.305mA$ & $10.77\mu A$ \\
            \rowcolor{white}
            $Q_2$ & $15V$ & $1.235V$ & $1.9V$ &$2.19mA$ & $2.206mA$ & $15.39\mu A$\\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{DC Operating Points for $Q_1$ and $Q_2$}
    \label{tab:bias}
\end{table}

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g}
            \toprule
            \rowcolor{white}
            & $g_m$ & $h_{FE}$ & $r_{\pi}$\\
            $Q_1$ & $0.0518S$ & $120.24$ & $2.32\text{k}\Omega$ \\
            \rowcolor{white}
            $Q_2$ & $0.0875S$ & $142.3$ & $1.624\text{k}\Omega$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{DC Operating Points for $Q_1$ and $Q_2$}
    \label{tab:small_sig}
\end{table}

The open-loop frequency response was then simulated, with the amplitude response being shown in Figure \ref{fig:open_loop}. The upper and lower 3dB frequencies 
were measured to be at $91.9$kHz and $2.876$Hz respectively. The open-loop gain was measured to be $-126.9V/V$.
\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/partC_openloop.png}
    \caption{Open-loop Amplitude Response}
    \label{fig:open_loop}
\end{figure}

The input and output resistances of the amplifier at $1$kHz were then measured with test sources at the input or output respectively. Measureing the input voltage and current gave an input resistance of $2.575\text{k}\Omega$, and the output resitance was measured to be $63.81\Omega$ as shown in Table \ref{tab:openR}.

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g}
            \toprule
            \rowcolor{white}
            $V_{in}$ & $I_{in}$ & $R_{in}$ & $V_{out}$ & $I_{out}$ & $R_{out}$ & $R_f$ \\
            $240.1\mu V$ & $93.23nA$ & $2.575\text{k}\Omega$ & $84.74mV$ & $1.328mA$ & $63.81\Omega$ & $\infty$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Open-loop Input and Output Resistances}
    \label{tab:openR}
\end{table}

As the topology was determined to be shunt-shunt, this is a transresistance amplifier. Therefore we use Y-parameters to make feedback calculations. $\beta$ corresponds to the parameter $y_{12}$, which can be found as follows,

\[ y_{12} = \frac{I_1}{V_2} \bigg\rvert_{V_1=0} = -\frac{1}{R_f} \]

Because we are working with the source resistance and load connected for all the gain measurements, our analysis is simplified to just the input and output voltages and currents, and not the gain of the isolated amplifier which is dependent on the voltage across its input resistance. To use the measured gain in feedback calculations, it must be in units of ohms. We therefore perform a source transformation on the input giving,

\[ A' = A_vR_s = \boxed{634.5\text{k}\Omega}\]

The gain with feedback can then be calculated as,

\begin{equation}
    A_f = \frac{A'}{1 + A'\beta}
    \label{eq:feedback}
\end{equation}

Given the shunt-shunt connection, we expect both the input and output resistances to reduced by a factor of $1 + A'B$. Similarly, we expect the midband to be extended, with the low 3dB decreasing by $1 + A'B$ and the high 3dB increasing by $1 + A'B$. Calculating the predicted vales for $A_V$, $f_{L3\text{dB}}$, $f_{H3\text{dB}}$, $R_{in}$, and $R_{out}$ with $R_f = 100\text{k}\Omega$ we get the following,

\[ A_V = \frac{A_f}{R_s} = \frac{A'}{R_s(1+A'\beta)} = \boxed{-17.28V/V}\]

\[ f_{L3\text{dB}} = \frac{2.876}{1 + A'B} = \boxed{0.3919\text{Hz}} \]

\[ f_{H3\text{dB}} = (91.9\times 10^3)(1 + A'B) = \boxed{675.47\text{kHz}} \]

\[ R_{in} = \frac{2.57\times10^3}{1 + A'B} = \boxed{350.34\Omega} \]

\[ R_{out} = \frac{63.81}{1 + A'B} = \boxed{8.68\Omega} \]

The closed-loop frequency response was then simulated for $R_f = 1\text{k}\Omega, 10\text{k}\Omega, 100\text{k}\Omega, 1\text{M}\Omega, \text{ and } 10\text{M}\Omega$. The amplitude response for each $R_f$ is presented in Figure \ref{fig:amplitudes}, where the trace with the largest magnitude is for $R_f = 10\text{M}\Omega$, and the smallest is $R_f = 1\text{k}\Omega$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{figures/partC_amplitueds.png}
    \caption{Amplitude Responses for Varying $R_f$}
    \label{fig:amplitudes}
\end{figure}

We wish to calculate the value for $\beta$ in each case. This can be done by solving \ref{eq:feedback} for $\beta$, which in terms of the measured voltage gains results in,

\begin{equation}
    \beta = \frac{1}{R_s} \left( \frac{1}{A_V} - \frac{1}{A_{V_{open-loop}}} \right)
    \label{eq:beta}
\end{equation}

The measured and calculated values of $\beta$ and $A_V$ for each $R_f$ are presented below in Table \ref{tab:gainbeta}

\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g}
            \toprule
            \rowcolor{white}
            $R_f$ & $1\text{k}\Omega$ & $10\text{k}\Omega$ & $100\text{k}\Omega$ & $1\text{M}\Omega$ & $10\text{M}\Omega$ \\
            $A_{V_{\text{calculated}}}$ & $-0.1997V/V$ & $-1.969V/V$ & $-17.28V/V$ & $-77.68V/V$ & $-119.42V/V$  \\
            \rowcolor{white}
            $A_{V_{\text{measured}}}$ & $-0.198V/V$ & $-1.960V/V$ & $-17.22V/V$ & $-77.79V/V$ & $-119.8V/V$ \\
            $\beta_{\text{calculated}}$ & $-1\times10^{-3}S$ & $-1\times10^{-4}S$ & $-1\times10^{-5}S$ & $-1\times10^{-6}S$ & $-1\times10^{-7}S$ \\
            \rowcolor{white}
            $\beta_{\text{measured}}$ & $-1.009\times10^{-3}S$ & $-1.005\times10^{-4}S$ & $-1.004\times10^{-5}S$ & $-9.96\times10^{-7}S$ & $-9.46\times10^{-8}S$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Measured and Calculated $A_V$ and $\beta$}
    \label{tab:gainbeta}
\end{table}

The measured upper and lower $3$dB points were $681.3$kHz and $0.498$Hz respectively. We can see that these values agree closely with the predicted frequencies calculated previously. Likewise, the measured values for $\beta$ are all very close to the predicted value of $-1/R_f$, and the measured gain closely matches the predicted. \\

The input and output resistances with feedback were measured for $R_f = 10\text{k}\Omega, 100\text{k}\Omega, \text{ and } 1\text{M}\Omega$. These values are presented in Table \ref{tab:resistanc_feedback}.

\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g}
            \toprule
            \rowcolor{white}
            $V_{in}$ & $I_{in}$ & $R_{in}$ & $V_{out}$ & $I_{out}$ & $R_{out}$ & $R_f$ \\
            $3.7\mu V$ & $140.4nA$ & $26.45\Omega$ & $7.055mV$ & $3.45mA$ & $2.03\Omega$ & $10\text{k}\Omega$ \\
            \rowcolor{white}
            $32.45\mu V$ & $134.6nA$ & $241.08\Omega$ & $7.055mV$ & $817.7\mu A$ & $8.63\Omega$ & $100\text{k}\Omega$ \\
            $146.3\mu V$ & $111.8nA$ & $1308.59\Omega$ & $7.055mV$ & $185.9\mu A$ & $37.95\Omega$ & $1\text{M}\Omega$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Measured Input and Output Resistances with Feedback}
    \label{tab:resistanc_feedback}
\end{table}

From the measured resistances, we can calculate the amount of feedback (AF), $1 + A'\beta$, knowing that both the input and output resistances should be reduced by this factor. Dividing the open-loop resistances by the resistances found with feed back will give the amount of feedback. Table \ref{tab:AF} shows these values.

\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g}
            \toprule
            \rowcolor{white}
            $R_f$ & AF (input) & AF (output) & AF (average) & AF (calculated)\\
            $10\text{k}\Omega$ & $64.5$ & $31.27$ & $47.89$ & $64.5$\\
            \rowcolor{white}
            $100\text{k}\Omega$ & $10.68$ & $7.39$ & $9.04$ & $7.35$ \\
            $1\text{M}\Omega$ & $1.97$ & $1.68$ & $1.82$ & $1.64$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Measured and Calculated Amount of Feedback}
    \label{tab:AF}
\end{table}

We can see from the above values, that with the exception of at $10\text{k}\Omega$, the value found from the output resistance is much closer the the predicted value. \\


To look at the desensitivity effect of feedback, the circuit was simulated again at both $R_f = \infty$ and $R_f = 100\text{k}\Omega$, while changing $R_C$ to the values $9.9\text{k}\Omega$, $10\text{k}\Omega$, and $10.1\text{k}\Omega$. Thevoltage gains in each case are presented in Table \ref{tab:desen}.

\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g}
            \toprule
            \rowcolor{white}
            $R_f = \infty$ & $R_C$ & $A_V$ & $R_f = 100\text{k}\Omega$ & $R_C$ & $A_V$ \\
            & $9.9\text{k}\Omega$ & $-126.8V/V$ & & $9.9\text{k}\Omega$ & $-17.25V/V$ \\ 
            \rowcolor{white}
            & $10\text{k}\Omega$ & $-127.3V/V$ & & $10\text{k}\Omega$ & $-17.25V/V$ \\ 
            & $10.1\text{k}\Omega$ & $-128.2V/V$ & & $10.1\text{k}\Omega$ & $-17.26/V$ \\ 
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Effect of Changing $R_C$ with and without Feedback}
    \label{tab:desen}
\end{table}

We see that the gain with feedback is left essentially unchanged as $R_C$ is varried. The desensitivity factor is another name for the amount of feedback, $1 + A'\beta$, which for $R_f = 100\text{k}\Omega$ is $7.35$ as found in Table \ref{tab:AF}. For $R_f = \infty$, $\beta = 0$ so the desensitivity factor is 1. \\

For small values of $R_f$, it can be seen from Figure \ref{fig:amplitudes} that there is a large spike in the gain around the low frequency cut-in. The filter response at the cut-in frequency is characterized by the pole quality factor $Q$. When $Q > 1/\sqrt{2}$, there will be a peak at the cut-in frequency. Therefore as $R_f$ decreases, so does $Q$. The values of $R_f$ which display a peak in the response cause $Q$ to be less than $1/\sqrt{2}$. 

\pagebreak
\section{References}
[1] A. Sedra, K. Smith ``Microelectronic Circuits'' \\
\noindent[2] N. A. F. Jaeger, ``CH22 Oscillators with Appendix''\\


\end{document}

