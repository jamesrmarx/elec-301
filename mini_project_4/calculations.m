% calculations for miniproject 4

%% Part A

R = 10e3; % Ohms
f = 10e3; % Hz
w3dB = f*2*pi; % rad/s
zeta = cos(pi/4);

C = 1/(R*w3dB);

Am = 3 - (2*zeta);

% resistor values for self-oscillation

R1 = 6.7e3;
R2 = 3.3e3;

Am_ = 1 + (R2/R1);