% this script assumes only real frequencies and the text file only has
% three columns, frequency, and then the real and imaginary parts of the 
% transfer function.
%
% Author: James Marx
% Edited: 2022.12.03

data_1k = readtable("partC_bode_1k.txt");
f_1k = data_1k.Var1; % get frequency
complexVals_1k = data_1k.Var2 + i.*data_1k.Var3; % re-combine to complex value
dB_1k = mag2db(abs(complexVals_1k));
phase_1k = rad2deg(angle(complexVals_1k));

data_10k = readtable("partC_bode_10k.txt");
f_10k = data_10k.Var1; % get frequency
complexVals_10k = data_10k.Var2 + i.*data_10k.Var3; % re-combine to complex value
dB_10k = mag2db(abs(complexVals_10k));
phase_10k = rad2deg(angle(complexVals_10k));

data_100k = readtable("partC_bode_100k.txt");
f_100k = data_100k.Var1; % get frequency
complexVals_100k = data_100k.Var2 + i.*data_100k.Var3; % re-combine to complex value
dB_100k = mag2db(abs(complexVals_100k));
phase_100k = rad2deg(angle(complexVals_100k));

data_1M = readtable("partC_bode_1M.txt");
f_1M = data_1M.Var1; % get frequency
complexVals_1M = data_1M.Var2 + i.*data_1M.Var3; % re-combine to complex value
dB_1M = mag2db(abs(complexVals_1M));
phase_1M = rad2deg(angle(complexVals_1M));

data_10M = readtable("partC_bode_10M.txt");
f_10M = data_10M.Var1; % get frequency
complexVals_10M = data_10M.Var2 + i.*data_10M.Var3; % re-combine to complex value
dB_10M = mag2db(abs(complexVals_10M));
phase_10M = rad2deg(angle(complexVals_10M));

% set cursor values 

%Yc = 51.48;
%Yd = 48.35;
%Xa = 4.88e6;
% Xb = 3.704e6;


% phase plot
figure
semilogx(f_10M, phase_10M, 'LineWidth',2)
hold on
semilogx(f_1M, phase_1M, 'LineWidth',2)
semilogx(f_100k, phase_100k, 'LineWidth',2)
semilogx(f_10k, phase_10k, 'LineWidth',2)
semilogx(f_1k, phase_1k, 'LineWidth',2)
hold off
grid on
ylabel('Phase (deg)','Interpreter','latex', 'FontSize', 16)
xlabel('Frequency (Hz)','Interpreter','latex', 'FontSize', 16)
xlim([10e-3,100e6])

% magnitude plot
figure
semilogx(f_10M, dB_10M, 'LineWidth',2)

hold on
semilogx(f_1M, dB_1M, 'LineWidth',2)
semilogx(f_100k, dB_100k, 'LineWidth',2)
semilogx(f_10k, dB_10k, 'LineWidth',2)
semilogx(f_1k, dB_1k, 'LineWidth',2)
hold off
ylabel('Magnitude (dB)','Interpreter','latex', 'FontSize', 16)
xlabel('Frequency (Hz)','Interpreter','latex', 'FontSize', 16)
legend('$R_f = 10$M','$R_f = 1$M','$R_f = 100$k','$R_f = 10$k','$R_f = 1$k', 'Interpreter', 'latex')
xlim([10e-3,100e6])

% magnitude plot cursors

%line([Xa,Xa],ylim, 'LineWidth', 1, 'LineStyle', '--', "Color", 'black')
% line([Xb,Xb],ylim, 'LineWidth', 1, 'LineStyle', '--', "Color", 'black')
% line(xlim, [Yc,Yc], 'LineWidth', 1, 'LineStyle', '--', "Color", 'black')
% line(xlim, [Yd,Yd], 'LineWidth', 1, 'LineStyle', '--', "Color", 'black')
grid on