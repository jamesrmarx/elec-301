Vcc = 12;
Ri = 50
b = 300;
VT = 25e-3;
Ic1 = (b*VT)/(Ri*(1+b));
Ie1 = Ic1;
I1 = 0.1*Ie1;
Ib1 = Ic1/b;
% externally soled with CAS
Re2 = 7572.97;
f = 1000


Rc1 = (1/3)*(Vcc/Ic1);
Re1 = Rc1;
Rb1 = (2/3)*(Vcc/I1);
Rb2 = (1/3)*(Vcc/(I1-Ib1));
rpi1 = (b*VT)/Ic1;
gm1 = b/rpi1;
Rbb = RR(Rb1,Rb2);
Rcb = RR(Rbb,(rpi1+(1+b)*Re1))

c = 1/(100*sqrt((2*pi*f)^2/2));

wL3dB = sqrt(2*(1/(100*c))^2); % just double checking

cb = (10000*c)/Rcb; % place 100x away

wL3dB_2 = sqrt(2*(1/(100*c))^2 + (1/(Rcb*cb)^2)); % just double checking



function rr = RR(r1,r2)
    rr = (r1*r2)/(r1+r2);
end