\providecommand{\main}{.}
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage[labelfont=bf]{caption}
\usepackage{subcaption}
\usepackage{setspace}
\usepackage{circuitikz}
\usepackage{amsmath}
\usepackage{float}
\usepackage{booktabs}
\usepackage{color, colortbl}

\definecolor{Gray}{gray}{0.9}

%\captionsetup[figure]{font = {stretch=1.35}}


\title{ELEC 301 Mini Project 3}
\author{James Marx \\ \\ 80562549}
\date{November 18, 2022
\\
\vspace{15cm}
\hspace{8cm} Signature \hrulefill}

\begin{document}

\maketitle
\newpage

\tableofcontents
\listoffigures
\listoftables

\newpage

\section{The Cascode Amplifier}

Figure \ref{fig:cascode_circuit} shows a cascode amplifier wich has been designed to meet the following specifications shown in Table \ref{table:cascode_spec}. The details of arriving at these component values are presented below. In all cases $r_o$ is considered to be infinite.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/part1_circuit.png}
    \caption{Cascode Amplifier}
    \label{fig:cascode_circuit}
\end{figure}

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g}
            \toprule
            \rowcolor{white}
            $R_{out}$ (value at mid band) & $R_{in}$ & $|A_v|$ (minimum value at mid band) & $f_L$ (maximum value for the low-f cut-in) \\
            $2.5$k$\Omega \pm 250\Omega$ & $5$k$\Omega$ & $50$ & $500$Hz\\ 
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Cascode Amplifier Specifications}
    \label{table:cascode_spec}
\end{table}

The first step was to bias the amplifier using the $1/4$ rule, where $V_{C2} = \frac{3}{4}V_{cc}$, $V_{C1} = \frac{1}{2}V_{cc}$, and $V_{E1} = \frac{1}{4}V_{cc}$. We also take $I_{R_{B1}} = 0.1I_{E2}$, $I_E = \frac{I_C}{\alpha}$ and $I_B = \frac{I_C}{\beta}$, where $\alpha = \frac{\beta}{1+\beta}$. As was done in miniproject 2, $\beta$ was determine from the Circuit Maker spice model to be $300$.  \\

The output resistance will be entirely from $R_C$ at mid band, so it is set as $2.5$k$\Omega$. We can now find the required currents for $Q_2$ and $Q_1$ to bias the transistors.

\[ I_{C2} = \frac{V_{cc} - V_{C2}}{R_C} = \boxed{2mA} \]
\[ I_{B2} = \frac{I_{C2}}{\beta} = \boxed{6.7\mu A} \]
\[ I_{E2} = \frac{I_{C2}}{\alpha} = \boxed{2.mA} \]
\[ I_{C1} = I_{E2} \]
\[ I_{B1} = \frac{I_{C1}}{\beta} = \boxed{6.8\mu A} \]
\[ I_{E1} = \frac{I_{C1}}{\alpha} = \boxed{2mA} \]

The values for the remaining resistors are determined by,

\[ R_{B1} = \frac{V_{cc} - V_{C1} + 0.7V}{0.1I_{E2}} = \boxed{46.4 \text{k} \Omega} \]
\[ R_{B2} = \frac{(V_{C1} +0.7V) - (V_{E1} + 0.7V)}{0.1I_{E2} - I_{B2}} = \boxed{25.7 \text{k} \Omega} \]
\[ R_{B3} = \frac{V_{E1} + 0.7V}{0.1I_{E2} - I_{B2} - I_{B1}} = \boxed{30.5 \text{k} \Omega} \]
\[ R_E = \frac{V_{E1}}{I_{E1}} = \boxed{2.48 \text{k} \Omega} \]

Using standard resistor values, the final values are presented in Table \ref{table:part1_standard}.

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g}
            \toprule
            \rowcolor{white}
            $R_C$ & $R_E$ & $R_{B1}$ & $R_{B2}$ & $R_{B3}$ \\
            $2.4 \text{k} \Omega$ & $2.4 \text{k} \Omega$ & $47 \text{k} \Omega$ & $27 \text{k} \Omega$ & $33 \text{k} \Omega$\\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Final Standard Resistor Values}
    \label{table:part1_standard}
\end{table}


In Circuit Maker, the DC operating point of the cascode amplifier was measured and presented in Tables \ref{table:part1_DC_current} and \ref{table:part1_DC_voltage}.


\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g}
            \toprule
            \rowcolor{white}
            & $I_{C1}$ & $I_{E1}$ & $I_{B1}$ & $I_{C2}$ & $I_{E2}$ & $I_{B2}$ \\
            Measured & $2.038mA$ & $2.054mA$ & $15.51\mu A$ & $2.023mA$ & $2.038mA$ & $15.38\mu A$ \\
            \rowcolor{white}
            Calculated & $2.02mA$ & $2.04mA$ & $17.39\mu A$ & $2mA$ & $2.02mA$ & $17.56\mu A$\\
            \% Error & $0.89$ \% & $0.69$ \% & $10.8$ \% & $1.15$ \% & $0.89$ \% & $12.4$ \% \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Cascode Amplifier DC Operating Point Currents}
    \label{table:part1_DC_current}
\end{table}

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g}
            \toprule
            \rowcolor{white}
            & $V_{C1}$ & $V_{E1}$ & $V_{B1}$ & $V_{C2}$ & $V_{E2}$ & $V_{B2}$ \\
            Measured & $9.920V$ & $4.926V$ & $5.592V$& $15.15V$ & $9.920$ & $10.59V$\\
            \rowcolor{white}
            Calculated & $10V$ & $5V$ & $5.7V$ & $15V$ & $10V$ & $10.7V$ \\
            \% Error & $0.8$ \% & $1.48$ \% & $1.90$ \% & $1$ \% & $0.8$ \% & $1.03$ \% \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Cascode Amplifier DC Operating Point Voltages}
    \label{table:part1_DC_voltage}
\end{table}



To find the remaining small signal parameters, we solve for $r_{\pi}$ and $g_m$ given by,

\[ r_{\pi 1} = \frac{\beta V_T}{I_{C1}} = \boxed{3.74 \text{k} \Omega} \]
\[ r_{\pi 2} = \frac{\beta V_T}{I_{C2}} = \boxed{3.75 \text{k} \Omega} \]
\[ g_{m1} = \frac{\beta}{r_{\pi 1}} = \boxed{80.1mS} \]
\[ g_{m2} = \frac{\beta}{r_{\pi 2}} = \boxed{80mS} \]

To find $c_{\pi}$ and $c_{\mu}$from the spice model we can use the following equations,


\begin{equation}
    c_{\pi} = 2\times CJE + TF\times g_m = \boxed{41pF} 
    \label{eq:cpi}
\end{equation}

\begin{equation}
    c_{\mu} = \frac{CJC}{(1+\frac{VCB}{VJC})^{MJC}} = \boxed{1.9pF}
    \label{eq:cmu}
\end{equation}


\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g}
            \toprule

            \rowcolor{white}
            $CJE$ & $TF$ & $CJC$ & $VJC$ & $MJC$\\
            $4.5\times10^{-12}$ & $400\times10^{-12}$ & $3.5\times10^{-12}$ & $750\times10^{-3}$ & $330\times10^{-3}$ \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{SPICE Parameters for 2n3904}
    \label{table:2n3904_SPICE}
\end{table}



To meet the requirement of a $5\text{k}\Omega$ input resistance at mid band, a series resistance needed to be added to the base of $Q_1$. The input resistance at midband is given by,

\[ R_{in} = R_{B2} || R_{B3} || r_{\pi 1} + R_{\text{added}} \]

Solving for $R_{\text{added}}$ gives a series resistance of $3.7 \text{k} \Omega$. However, after running simulations with the finalized values, it was determined that a smaller resistance of $3.3\text{k}\Omega$ could be used and still meet the specification. \\

To determine the capacitor values to meet the specification of a maximum low frequency cut in of $500$Hz, we will consider the pole associated with $C_E$, as this is likely to contribute the dominant low frequency pole. For an economic design, we wish to use the smallest capacitors possible, so the capacitor value wich produces a pole as close to $500$Hz without going over will be selected. Figure \ref{fig:part1_lowf} shows the low frequency small signal model necessary to analyze $C_E$.

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_S$] (2,2);
        \draw (2,2) to [R=$R_{\text{add}}$] (4,2);
        \draw (4,2) to [C=$C_{c1}$] (6,2);
        \draw (6,2) to [short] (10,2);
        \draw (7,2) to [R=$R_{B2}||R_{B3}$] (7,0);
        \draw (7,0) to (7,0) node[ground]{};
        \draw (10,2) to [R=$r_{\pi 1}$] (10,0);
        \draw (10,0) to [short] (14,0);
        \draw (14,2) to [cI] (14,0);
        \draw (12,0) to [short] (12,-1);
        \draw (12,-1) to [short] (11,-1);
        \draw (11,-1) to [R=$R_E$] (11,-3);
        \draw (11,-3) to (11,-3) node[ground]{};
        \draw (12,-1) to [short] (13,-1);
        \draw (13,-1) to [C=$C_E$] (13,-3);
        \draw (13,-3) to (13,-3) node[ground]{};
    \end{circuitikz}
    
    \caption{Low Frequency Model $Q_1$}
    \label{fig:part1_lowf}
\end{figure}

 Since we have determined it will be the dominant pole, we find its short circuit time constant.

This is easily done by de-magnifying the resistance seen looking back from the emitter.

\[ \omega_{LP_{3}} = \frac{1}{ \left( \frac{(R_S + R_{\text{add}})||R_{B2}||R_{B3} + r_{\pi 1}}{1+\beta}||R_E \right) C_E} \]

Replacing $\omega_{LP_{3}}$ with $2\pi \times 500$ rad/s and solving for $C_E$ we get,

\[ C_E = \boxed{8.6\mu \text{F}} \]

As this is not a standard capacitor value, we pick the closest (larger) capacitor for $C_E$ and the coupling capacitors -- $10\mu$F. Calculating the pole frequency for this new value gives, 

\[ \omega_{LP_3} = \boxed{2831.14\text{rad/s}} \]

There is also a low-frequency zero associated with $C_E$ given by,

\[ \omega_{LZ_3} = \frac{1}{C_E R_E} = \boxed{41.67\text{rad/s}} \]

The pole for $C_{c1}$ is found with an open circuit time constant and magnifying the emitter resistance,

\[ \omega_{LP_2} = \frac{1}{\left( R_S + R_{\text{add}} + R_{B2}||R_{B3}||(r_{\pi 1} + R_E(1+\beta)) \right)C_{c1}} = \boxed{5.73\text{rad/s}} \]

Finally the pole associated with $C_{c2}$ is simply given by,

\[ \omega_{LP_1} = \frac{1}{C_{c2}(R_C + R_L)} = \boxed{1.908\text{rad/s}} \]

Here we simply present the final form of the high frequency pole approximations, for full derivations please refer to [1]. The poles are given by,

\[ \omega_{HP_1} = \frac{1}{(r_{\pi 1}||R_{B2}||R_{B3}||(R_S + R_{\text{add}}))(c_{\pi} + 2c_{\mu})} = \boxed{1.76\times10^{7}\text{rad/s}}  \]

\[ \omega_{HP_2} = \frac{1}{c_{\mu}(R_C||R_L)} = \boxed{2.26\times10^8\text{rad/s}} \]

\[ \omega_{HP_3} = \frac{1}{(c_{\pi} + 2c_{\mu}|)\frac{r_{pi2}}{1+\beta}} = \boxed{1.80\times10^9\text{rad/s}} \]

Now we find the $3$dB points,

\[ \omega_{L3dB} = \sqrt{\omega_{LP_1}^2 + \omega_{LP_2}^2 + \omega_{LP_3}^2 - 2\omega_{LZ_3}^2}  = \boxed{2830.5\text{rad/s}}\]

\[ \omega_{H3dB} = \frac{1}{\sqrt{\tau_{HP_1}^2 + \tau_{HP_2}^2 + \tau_{HP_3}^2  }} = \boxed{1.75\times10^7\text{rad/s}} \]

Figures \ref{fig:part1_mag} and \ref{fig:part1_phase} show the simulated magnitued and phase response of the cascode amplifier. From the magnitude plot, we can compare the calculated 3dB points with the simulation, these results are summarized in Table \ref{table:3db}.

\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g}
            \toprule
            \rowcolor{white}
            & $f_{L3dB}$ & $f_{H3dB}$ \\
            Calculated & $450.5$Hz & $2.79$MHz \\
            \rowcolor{white}
            Measured & $496.7$Hz & $3.51$MHz \\
            \% Error & $10.3$ \% & $25.8$ \% \\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Cascode Amplifier DC Operating Point Voltages}
    \label{table:3db}
\end{table}



\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part1_mag.png}
    \caption{Cascode Magnitude Response Determining Low and High 3dB Points}
    \label{fig:part1_mag}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part1_phase.png}
    \caption{Cascode Phase Response}
    \label{fig:part1_phase}
\end{figure}

At a midband frequency of $10$kHz, the input voltage was varied until the output became non-linear. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/Av_plot.png}
    \caption{Voltage Transfer Curve}
    \label{fig:vt_curve}
\end{figure}


It can be seen from Figure \ref{fig:vt_curve} that as the input pushes toward $70-80mV$, it begins to stray from a linear relation. In the linear region, the slope -- $A_v$ -- is $55V/V$,  meeting the specification. Viewing the time domain signal at $70mV$, it can be seen to longer resemble a perfect sinusoid. This is shown in Figure \ref{fig:70mV}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/Vo_70mV.png}
    \caption{Output Voltage for $V_S = 70mV$}
    \label{fig:70mV}
\end{figure}

Finally the input and output resistances were calculated to ensure they were within the specification. At $1mV$ and $10$kHz, the input current was found to be $120.5nA$(RMS) and the input voltage was $614.7\mu V$(RMS), giving an input resistance $R_{\text{in}} = 5.1\text{k}\Omega$. To find the output resistance, the input source was shorted and a test source applied to the output at $1V$ and $10$kHz. The output current was $258.7\mu A$(RMS) and the output voltage $620.7mV$ (RMS), giving an ouptut resistance $R_{\text{out}} = 2.4\text{k}\Omega$.

\pagebreak
\section{Cascaded Amplifiers}

The circuit shown in figure \ref{fig:part2_circuit} is to be designed as an analog repeater in a $50\Omega$ coaxial cable system. It is to have an input and output impedance of $50 \pm 50\Omega$ at mid band. The value for $\beta$ was found from the Circuit Maker SPICE model to be $300$.\\

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/part2_circuit.PNG}
    \caption{Common-base/Common-Collector Repeater}
    \label{fig:part2_circuit}
\end{figure}

To find the component values shown in the circuit diagram, the circuit was first biased with the 1/3 rule such that $V_{E1} = V_{cc}$ and $I_1 = 0.1I_{E1}$, with the requirement that the low-frequency cut-in point is at, or below, $1000$Hz. To apply this rule, we need a value for $I_{C1}$ which facilitates the design, so we will solve for that first. Given the requirement of the input resistance we can solve for this current. Looking into the emitter at mid band we find the equivalent input resistance to be,

\[ R_i = R_{E1}||\frac{r_{\pi 1}}{1+\beta} \text{ ; where $r_{\pi 1} = \frac{\beta V_T}{I_{C1}}$} \]

Assuming a large $R_{E1}$ we get,

\[ I_{C1} = \frac{\beta V_T}{R_i (1+\beta)} = \boxed{0.499mA} \]

Giving values for the $Q_1$ bias network to be,

\[ R_{C1} = R_{E1} = \frac{1}{3} \frac{V_{cc}}{I_{C1}} = \boxed{8.03\text{k}\Omega} \]
\[ R_{B1} = \frac{2}{3} \frac{V_{cc}}{I_1} = \boxed{160.5\text{k}\Omega} \]
\[ R_{B2} = \frac{1}{3} \frac{V_{cc}}{I_1 - I_{B1}} = \boxed{83.0\text{k}\Omega} \]

To determine $R_{E2}$ we apply a similar technique as used to find $I_{C1}$. Again, looking into the emitter of $Q_2$ this time we find,

\[ R_o = R_{E2} || \left( \frac{r_{\pi 2 + R_{C1}}}{1+\beta} \right) \]

Substituting for $r_{\pi2}$ we get,

\[ R_o = R_{E2} || \left( \frac{R_{C1}}{1+\beta} \frac{R_{E2} V_T}{V_{C1} - 0.7V}  \right) \]

We can use CAS software to solve for $R_{E2} = 7.57 \text{k} \Omega$. \\

To solve for the capacitor values, we will take advantage of the fact $C_{C1}$ and $C_{C2}$ see essentially the same resistance and design them to have the same value and both contribute to the dominant low-frequency pole. For the time being we will ignore  $C_B$, and then later design it to not effect the location of the dominant pole. The short circuit time constant for $C_{C1}$ will be the resistance seen looking into the emitter of $Q_1$ in series with the source resistance,

\[ \tau_{C_{C1}} = \left( \frac{r_{\pi 1}}{1+\beta} || R_{E1}  + R_S\right)C_{C1} \approx 100C_{C1} \]

And the time constant for $C_{C2}$ is simply the series combination of the output resistance of $Q_2$ and the load resistance,

\[ \tau_{C_{C2}} = (R_o + R_L)C_{C2} \approx 100C_{C2} \]

Solving for the low-frequency 3dB point at the given frequency results in,

\[ 2\pi f_{3dB} = \sqrt{2 \left( \frac{1}{100C} \right)^2 }  \Rightarrow C = \frac{1}{100\sqrt{\frac{(2\pi f_{3dB})^2}{2}}} = \boxed{2.251\mu F} \]

To design $C_B$, we first find the equivalent resistance it sees at midband to get its associated open circuit time constant. This will be the the parallel bias resistors in parallel with the resistance seen looking into the base of $Q_1$,

\[ \tau_{C_B} = (R_{BB}||(r_{\pi 1} + (1+\beta)R_{E1}))C_B \]

To not impact the dominant pole, we wish $\tau_{C_{C1}} \ll \tau_{C_B}$, so we will assume $\tau_{C_B} = 100\times \tau_{C_{C1}}$, giving,

\[ C_{B} = \frac{10000C_{C1}}{(R_{BB}||(r_{\pi 1} + (1+\beta)R_{E1}))} = \boxed{42\mu F} \]

Picking available values for resistors and capacitors, we arrive at the component values summarized in table \ref{table:part2_components}.


\newcolumntype{g}{>{\columncolor{Gray}}c}
\begin{table}[H]
    \begin{center}
        \begin{tabular}{g g g g g g g g}
            \toprule
            \rowcolor{white}
            $R_{B1}$ & $R_{B2}$ & $R_{C1}$ & $R_{E1}$ & $R_{E2}$ & $C_{C1}$ & $C_{C2}$ & $C_B$ \\
            $160$k$\Omega$ & $82$k$\Omega$ & $8.2$k$\Omega$ & $8.2$k$\Omega$ & $7.5$k$\Omega$ & $2.2\mu F$ & $2.2\mu F$ & $47\mu F$\\
            \bottomrule
        \end{tabular}
    \end{center}
    \caption{Chosen Component Values for the Cascaded Amplifiers}
    \label{table:part2_components}
\end{table}

To ensure the required input and output resistances were met, the circuit was created in Circuit Maker without the source or load resistances and simulated at a mid band frequency of $100$kHz. To meet the input requirement, $R_{B2}$ was adjusted to $100\text{k}\Omega$ and $R_{E1}$ to $7.5\text{k}\Omega$. This gave an measured input resistance of $55.0\Omega$. To meet the output requirement, $R_{C1}$ was adjusted to $6.2\text{k}\Omega$ and $R_{E2}$ to $5.1\text{k}\Omega$, giving a measured output resistance of $51.2\Omega$. The mid band voltage gain $A_M$ was measured to be $110V/V$. \\

The source and load impedances were then added and the capacitor values adjusted until the cut-in frequency specification was met, with the intention to use the smallest component values possible. By setting $C_{C2}$ to $2.7\mu F$, $C_B$ was able to be reduced to $0.18\mu F$ and give a low 3dB point of $1.00$kHz and a high 3dB of $3.7$MHz. Figures \ref{fig:part2_mag} and \ref{fig:part2_phase} show the magnitude and phase response for the final circuit.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part2_mag.png}
    \caption{Magnitude Response Determining low and high 3dB Points}
    \label{fig:part2_mag}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part2_phase.png}
    \caption{Phase Response}
    \label{fig:part2_phase}
\end{figure}

\pagebreak
\section{Differential Amplifier}

To design the differntial amplifier shown in Figure \ref{fig:part3_circuit} we will begin by finding the value of $R_{REF}$ given that $I_E = 1mA$ for $Q_1$ and $Q_2$. Again we take $\beta$ to be 300 from the SPICE model. For the current mirror, $I_o$ will be $2I_E$, so we solve for $I_{REF}$ to find $R_{REF}$ as shown below.

\[ I_{REF} = I_o \left( 1 + \frac{2}{\beta} \right) = \boxed{2.012mA} \]

\[ R_{REF} = \frac{0 - (V_E + 0.7V)}{I_{REF}} = \boxed{7.103\text{k}\Omega} \]

\begin{figure}[H]
    \centering
    \includegraphics[width=0.6\linewidth]{figures/part3_circuit.PNG}
    \caption{Differential Amplifier}
    \label{fig:part3_circuit}
\end{figure}


Using the closest available resitor of $6.8\text{k}\Omega$, we simulate the circuit above and get the resulting magnitude and phase plots shown in Figures \ref{fig:part3_mag} and \ref{fig:part3_phase}. To compare the simulated high-3dB to calculated, we first find values for the junction capacitances based on the SPICE model. We need $V_{CB}$, so we will assume at the DC operating point $I_C = I_E$, and solve for $V_C$ given the base of $Q_2$ is grounded.

\[ V_C = I_C R_C - 15V = -5V  \Rightarrow V_{CB} = \boxed{5V} \]

The junction capacitances are then calculated with equations \ref{eq:cpi} and \ref{eq:cmu} giving $24.8pF$ for $c_{\pi}$ and $1.79pF$ for $c_{\mu}$. The high frequency poles and the high-3dB point are calculated as shown below.

\[ k = -g_m R_C = \boxed{-400} \]

\[ \omega_{HP1} = \frac{1}{\left( \frac{c_{\pi}}{2} + \frac{c_{\mu}}{2}(1-k) \right) 2r_{\pi}||R_S} = \boxed{5.413\times10^7 \text{rad}/s} \]

\[ \omega_{HP2} = \frac{ 1 }{ \frac{ c_{\mu} }{ 2 }\left( 1 - \frac{1}{k} \right)2R_C } = \boxed{5.582\times10^7\text{rad}/s} \]

\[ \omega_{H3\text{dB}} = \frac{1}{ \sqrt{ \left(\frac{1}{\omega_{HP1}}\right)^2 + \left( \frac{1}{\omega_{HP2}} \right)^2 } } = 3.89\times10^7\text{rad}/s \Rightarrow f_{H\text{3dB}} = \boxed{6.18\text{MHz}} \]

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part3_mag.png}
    \caption{Magnitude Response Determining High 3dB Point}
    \label{fig:part3_mag}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part3_phase.png}
    \caption{Phase Response}
    \label{fig:part3_phase}
\end{figure}

From the simulated response, the high-3dB point was found to be $4.88$MHz, giving a percentage error of $26.6$\%. This error is likely due to the high $\beta$ assumed and the assumpions made to calculate $V_{CB}$ to be $5V$. \\

Finally, the linearity of input to output voltage was considered. Using a midband frequency of $1$kHz, the input voltage was varied from $0.5mV$ to $30.5mV$. Looking at the time-domain waveform, visual distortion of the output could be seen at and beyondan input voltage of $22.5mV$. The linear relation breaking beyond this point can be seen by Figure \ref{fig:part3_plot}. 

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part3_plot.png}
    \caption{Input Output Voltage Relation}
    \label{fig:part3_plot}
\end{figure}

\pagebreak
\section{AM Modulator}

Figure \ref{fig:part4_circuit} shows an AM modulator. This circuit has a $100$kHz carrier signal and a $1$kHz input signal. The amplitude of the carrier signal was varied between $10 - 100mV$ peak, output waveforms of interest for both sine and square wave inputs are shown in figures \ref{fig:part4_sine} and \ref{fig:part4_square} respectively. For the sinusoidal signal, after input voltages of around $65mV$, distortion and clipping of the output signal became significant. For the square wave, beyond approximately $80mV$ the output became saturated around $4.1V$.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.4\linewidth]{figures/part4_circuit.PNG}
    \caption{AM Modulator}
    \label{fig:part4_circuit}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part4_sine.png}
    \caption{Sinusoidal Outputs}
    \label{fig:part4_sine}
\end{figure}

\begin{figure}[H]
    \centering
    \includegraphics[width=0.8\linewidth]{figures/part4_square.png}
    \caption{Square Wave Outputs}
    \label{fig:part4_square}
\end{figure}

From the output figures, we see that the time domain input and carrier signals are multiplied together, which is equivalent to convolution in the frequency domain. As a result of this, we can take advantage of the higher frequency carrier signal to transmit the input greater distances, while also having the practical advantage of a physically smaller antenna to recieve the transmission with. The recieved signal can then be demodulated to recover the original input.


\pagebreak
\section{References}
[1] N. A. F. Jaeger, ``The Differential Amplifier''\\

\noindent[2] N. A. F. Jaeger, ``The Differential Amplifier's Frequency Response''\\

\end{document}

