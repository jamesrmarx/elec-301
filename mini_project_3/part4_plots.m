% import all data and get correct differential voltages

sine50_raw = readtable("part4_s_50.txt");
sine10_raw = readtable("part4_s_10.txt");
sine65_raw = readtable("part4_s_65.txt");
sine100_raw = readtable("part4_s_100.txt");

sine50 = sine50_raw.Var2 - sine50_raw.Var3;
sine10 = sine10_raw.Var2 - sine10_raw.Var3;
sine65 = sine65_raw.Var2 - sine65_raw.Var3;
sine100 = sine100_raw.Var2 - sine100_raw.Var3;

square50_raw = readtable("part4_sq_50.txt");
square80_raw = readtable("part4_sq_80.txt");
square100_raw = readtable("part4_sq_100.txt");

square50 = square50_raw.Var2 - square50_raw.Var3;
square80 = square80_raw.Var2 - square80_raw.Var3;
square100 = square100_raw.Var2 - square100_raw.Var3;

%% Sine Figures
subplot(2,2,1)
plot(sine50_raw.Var1, sine50, 'Color', 'black')
xlim([0.2, 0.2026])
title('50mVp Input', 'Interpreter','latex', 'FontSize', 16)
xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

subplot(2,2,2)
plot(sine10_raw.Var1, sine10, 'Color', 'black')
xlim([0.2, 0.2026])
title('10mVp Input', 'Interpreter','latex', 'FontSize', 16)
% xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
% ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

subplot(2,2,3)
plot(sine65_raw.Var1, sine65, 'Color', 'black')
xlim([0.2, 0.2026])
title('65mVp Input', 'Interpreter','latex', 'FontSize', 16)
% xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
% ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

subplot(2,2,4)
plot(sine100_raw.Var1, sine100, 'Color', 'black')
xlim([0.2, 0.2026])
title('100mVp Input', 'Interpreter','latex', 'FontSize', 16)
% xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
% ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

%% Square Figures
figure
subplot(2,2,1)
plot(square50_raw.Var1, square50, 'Color', 'black')
xlim([0.2, 0.2026])
title('50mVp Input', 'Interpreter','latex', 'FontSize', 16)
xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

subplot(2,2,2)
plot(square80_raw.Var1, square80, 'Color', 'black')
xlim([0.2, 0.2026])
title('80mVp Input', 'Interpreter','latex', 'FontSize', 16)
% xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
% ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)

subplot(2,2,[3,4])
plot(square100_raw.Var1, square100, 'Color', 'black')
xlim([0.2, 0.2026])
title('100mVp Input', 'Interpreter','latex', 'FontSize', 16)
% xlabel('time (s)', 'Interpreter','latex', 'FontSize', 16)
% ylabel('Output Voltage (V)', 'Interpreter','latex', 'FontSize', 16)