RC = 2.5e3;
b = 300;
a = b/(b+1);
IC2 = (20-15)/RC;
IC1 = IC2/a;
IE2 = IC2;
VT = 25e-3;
rpi1 = (b*VT)/IC1;
rpi2 = (b*VT)/IC2;
gm1 = b/rpi1;
gm2 = b/rpi2;
IB1 = IC1/b;
IB2 = IC2/b;
IE1 = IC1/a;

RB1 = (20-10.7)/(0.1*IC1);
RB2 = (10.7-5.7)/(0.1*IE2-IB2);
RB3 = (5.7)/(0.1*IE2 - IB2 - IB1);
RE = 5/IE1;
RBB = RR(RB2,RB3);

CJE = 4.4e-12;
TF = 400e-12;
CJC = 3.5e-12;
VJC = 750e-3;
MJC = 330e-3;

V_CB = 9.920-5.592;

c_pi = 2*CJE + TF*gm1
c_u = CJC/( (1 + V_CB/VJC)^MJC )

wHP1 = 1/(RR(rpi1,RR(RBB,50+2200))*(c_pi+2*c_u))
wHP2 = 1/((c_pi+2*c_u)*(rpi2/(1+b)))
wHP3 = 1/(c_u*RR(RC,50e3))

wH3dB = 1/sqrt( (1/wHP1)^2 + (1/wHP2)^2 + (1/wHP3)^2 )
fH3dB = wH3dB/(2*pi)

function rr = RR(r1,r2)
    rr = (r1*r2)/(r1+r2);
end