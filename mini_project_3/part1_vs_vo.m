% vector of voltage input (peak)
Vs = [0.5, 2.5, 4.5, 6.5, 8.5, 10.5, 12.5, 14.5, 16.5, 18.5, 20.5, 22.5, 24.5, 28.5, 30.5]*1e-3;
% vector of voltage output (RMS)
Vo = [136.4e-3, 681.8e-3, 1.226, 1.769, 2.309, 2.846, 3.379, 3.908, 4.431, 4.948, 5.449, 5.828, 6.109, 6.54, 6.718];
% multiply by sqrt(2) to get peak
Vo = Vo*sqrt(2);

plot(Vs, Vo, '-square', 'LineWidth', 1.5, 'Color', 'black')
ylabel('$V_o$ (V)', 'FontSize', 16, 'Interpreter','latex')
x   label('$V_s$ (V)', 'FontSize', 16, 'Interpreter','latex')
grid on

%%

data = readtable("part1_vo_70mV.txt");
time = data.Var1;
Vo_70 = data.Var2;

plot(time, Vo_70, 'LineWidth', 2, 'Color', 'black')
xlim([0, time(end)/3])
xlabel('time (s)', 'FontSize', 16, 'FontWeight', 'bold', 'Interpreter','latex')
ylabel('$V_o$ (V)', 'FontSize', 16, 'Interpreter','latex')
grid on