clear;

I_E1 = 1e-3;
R_C = 10e3;
VT = 25e-3;
I_E2 = I_E1;
I_o = 2*I_E1;
b = 300;
I_REF = I_o*(1+2/b);
V_E = -15;
V_BE = 0.7;
R_REF = (0-(V_E + V_BE))/I_REF;
R_S = 50;
V_CB = -(I_E1*R_C - 15)

rpi = (b*VT)/I_E1;
gm = b/rpi;
k = -gm*R_C;
CJE = 4.4e-12;
TF = 400e-12;
CJC = 3.5e-12;
VJC = 750e-3;
MJC = 330e-3;

c_pi = 2*CJE + TF*gm
c_u = CJC/( (1 + V_CB/VJC)^MJC )




wHP1 = 1/( ( c_pi/2 + (c_u/2)*(1-k) )*RR(2*rpi,R_S) );
wHP2 = 1/( ( (c_u/2)*(1-1/k) )*2*R_C );

w3dB = 1/( sqrt( (1/wHP1)^2 + (1/wHP2)^2 ) )
f3dB = w3dB/(2*pi)





function rr = RR(r1,r2)
    rr = (r1*r2)/(r1+r2);
end