\providecommand{\main}{.}
\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[margin=1in]{geometry}
\usepackage{wrapfig}
\usepackage{graphicx}
\usepackage[labelfont=bf]{caption}
\usepackage{subcaption}
\usepackage{setspace}
\usepackage{circuitikz}
\usepackage{amsmath}
\usepackage{float}

%\captionsetup[figure]{font = {stretch=1.35}}


\title{ELEC 301 Mini Project 1}
\author{James Marx \\ \\ 80562549}
\date{October 7, 2022
\\
\vspace{15cm}
\hspace{8cm} Signature \hrulefill}

\begin{document}

\maketitle
\newpage

\tableofcontents
\listoffigures
\listoftables

\newpage

\section*{Introduction}

The method of open circuit/short circuit time constants is an efficient method for approximating the pole locations of a circuit. In the event that a circuit's frequency response forms that of a band-pass filter, we can separate the frequency response into three components: the midband response($A_M$), the low frequency response($F_L$), and the high frequency response($F_H$). This gives a generalized form for the circuits transfer function to be,

\begin{equation}
    T(s) = A_M F_L(s) F_H(s)
    \label{eqn:general_response}
\end{equation}\\

By considering the circuit of interest at these three frequency ranges, we can independently find $A_M$, $F_L$, and $F_H$, then cascade the responses to build a transfer function which closely matches the response of our actual circuit. 


\section{Part I}

The circuit shown in figure \ref{fig:part1_circuit} is said to have the following transfer function,

\[
T(s) = \frac{V_o(s)}{V_s(s)} = 0.125\times \frac{5\times10^5}{s+5\times10^5} \times \frac{5\times10^6}{s+5\times10^6} \times \frac{5\times10^7}{s+5\times10^7}
\] \\

To determine the values of $C_1$, $C_2$ and $C_3$ (given that $C_1>C_2>C_3$), while using four $500\Omega$ and two $1000\Omega$ resistors, we employ the method of open circuit/short circuit time constants. Looking at the given transfer function, we recognize its form to be that of a high-pass filter.\\

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,2) to [R=$R_1$] (4,2);
        \draw (4,2) to [C=$C_1$] (4,0);
        \draw (4,2) to [short] (6,2);
        \draw (6,2) to [R=$R_4$] (6,0);
        \draw (6,2) to [R=$R_2$] (10,2);
        \draw (10,2) to [short] (12,2);    
        \draw (10,2) to [C=$C_2$] (10,0);
        \draw (12,2) to [R=$R_5$] (12,0);
        \draw (12,2) to [R=$R_3$] (16,2);
        \draw (16,2) to [short] (18,2);
        \draw (16,2) to [C=$C_3$] (16,0);
        \draw (18,2) to [R=$R_6$] (18,0);
        \draw (18,2) to [short,-o] (20,2);
        \draw (0,0) to [short, -o] (20,0);
        \draw (20,2) to [open, v=$V_o$] (20,0);
        \draw (10,0) to (10,0) node[ground]{};
    \end{circuitikz}
    
    \caption{Part I: Three-pole Low-pass Filter}
    \label{fig:part1_circuit}
\end{figure}

From \eqref{eqn:general_response} we see that $A_M = 0.125$, and $F_L = 1$. In the midband frequency range, all capacitors will be open circuits. We can re-draw the circuit as shown in figure \ref{fig:part1_midband} and consider the midband response to be,

\[
A_M = \frac{V_o}{V_s} = \frac{V_o}{V_2} \times \frac{V_2}{V_1} \times \frac{V_1}{V_s} = 0.125
\]


\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,2) to [R=$R_1$] (4,2);
        \draw (4,2) to [short] (6,2);
        \draw (6,2) to [R=$R_4$] (6,0);
        \draw (6,2) to (6,2) node[label=$V_1$]{};
        \draw (6,2) to [R=$R_2$] (10,2);
        \draw (10,2) to [short] (12,2);    
        \draw (12,2) to [R=$R_5$] (12,0);
        \draw (12,2) to (12,2) node[label=$V_2$]{};
        \draw (12,2) to [R=$R_3$] (16,2);
        \draw (16,2) to [short] (18,2);
        \draw (18,2) to [R=$R_6$] (18,0);
        \draw (18,2) to [short,-o] (20,2);
        \draw (0,0) to [short, -o] (20,0);
        \draw (20,2) to [open, v=$V_o$] (20,0);
        \draw (10,0) to (10,0) node[ground]{};
    \end{circuitikz}
    
    \caption{Part I: Filter in Midband Frequency Range}
    \label{fig:part1_midband}
\end{figure}

If each stage contributes a factor of $0.5$, it can be verified that setting $R_1=R_2=R_3=R_6=500\Omega$ and $R_4=R_5=1000\Omega$ solves the midband.\\

We see from the given transfer function that $C_1$ will contribute a pole at $\omega_{p1} = 5\times10^5 rad/s$, $C_2$ a pole at $\omega_{p2} = 5\times10^6 rad/s$ and $C_3$ a pole at $\omega_{p3} = 5\times10^7rad/s$. Starting from $C_1$, we assume the other smaller capacitors are open circuits and find the equivalent resistance ($R_1$) seen by $C_1$. For $C_2$ we assume $C_1$ is now a short and $C_3$ is an open circuit. Finally for $C_3$, both $C_1$ and $C_2$ are short circuits. Knowing that $\omega = 1/\tau$, where $\tau = CR$, we can solve for $C$. These results are summarized in Table \ref{tabel:1}.\\

\begin{table}[H]
    \begin{center}
        \begin{tabular}{c c c}
        \hline
        Given $\omega$ ($rad/s$) & Calculated $R_{n}$ & Calculated $C_n$\\
        \hline
        $5\times10^5$ & $250\Omega$ & $8nF$ \\
        $5\times10^6$ & $250\Omega$ & $0.8nF$\\
        $5\times10^7$ & $250\Omega$ & $80pF$\\
        \hline
             
        \end{tabular}
    \end{center}
    \caption{Part I: Calculated Capacitor Values}
    \label{tabel:1}
\end{table}

The circuit was then modeled in Circuit Maker to run AC simulations as shown in figure \ref{fig:part1_CMcircuit}. The results of simulation are shown in figures \ref{fig:part1_circuit_mag} and \ref{fig:part1_circuit_phase} as magnitude and phase Bode plots.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{partI_figures/part_I_circuit.png}
    \caption{Part I: Circuit Modeled in Circuit Maker}
    \label{fig:part1_CMcircuit}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partI_figures/part1_circuit_magnitude.png}
        \caption{Part I: Circuit Magnitude Bode Plot (X=frequency(HZ) Y=voltage(dB))}
        \label{fig:part1_circuit_mag}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partI_figures/part1_circuit_phase.png}
        \caption{Part I: Circuit Phase Bode Plot (X=frequency(HZ) Y=voltage(deg))}
        \label{fig:part1_circuit_phase}
\end{figure}

The given transfer function was then modeled in Circuit Maker, as shown in figure \ref{fig:part1_CMsxfer}, to compare its response to that of the circuit. Figures \ref{fig:part1_sxfer_mag} and \ref{fig:part1_sxfer_phase} show the magnitude and phase response of the transfer function compared to those of the circuit. It can be clearly seen that the circuit and transfer function plots agree almost perfectly.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{partI_figures/part_I_sxfer.png}
    \caption{Part I: Transfer Function Modeled in Circuit Maker}
    \label{fig:part1_CMsxfer}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partI_figures/part1_sxfer_magnitude.png}
        \caption{Part I: Circuit and Transfer Function Magnitude Bode Plot (X=frequency(HZ) Y=voltage(dB)) \\ \centering A: Circuit, B: Transfer Function}
        \label{fig:part1_sxfer_mag}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partI_figures/part1_sxfer_phase.png}
        \caption{Part I: Circuit and Transfer Function Phase Bode Plot (X=frequency(HZ) Y=voltage(deg)) \\ \centering A: Circuit, B: Transfer Function}
        \label{fig:part1_sxfer_phase}
\end{figure}

\pagebreak

\section{Part II}

Figure \ref{fig:part2_circuit} shows a four pole band pass filter, whose component values are summarized in Table \ref{tabel:2}. The method of open circuit/short circuit time constants was applied to find the transfer function of the given filter. Initially, we can identify that $C_1$ and $C_3$ will contribute poles at low frequencies, while $C_2$ and $C_4$ contribute poles at high frequencies. The value of $C_3$ was increased from $500nF$ to $1\mu F$, $2\mu F$, $5\mu F$, and $10\mu F$, with the resulting calculated responses being compared to simulated values.\\

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_i$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_1$] (2,2);
        \draw (2,2) to [C=$C_1$] (4,2);
        \draw (4,2) to [R=$R_2$] (4,0);
        \draw (4,2) to [short] (6,2);
        \draw (6,2) to [C=$C_2$] (6,0);
        \draw (6,0) to (6,0) node[ground]{};
        \draw (4,0) to (4,0) node[ground]{};
        \draw (6,2) to [R=$R_3$] (8,2);
        \draw (8,2) to [C=$C_3$] (10,2);
        \draw (10,2) to [R=$R_4$] (10,0);
        \draw (10,0) to (10,0) node[ground]{};
        \draw (10,2) to [short] (12,2);
        \draw (12,2) to [C=$C_4$] (12,0);
        \draw (12,0) to (12,0) node[ground]{};
        \draw (12,2) to [short,-o] (14,2);
        \draw (14,0) to [short,-o] (14,0);
        \draw (14,0) to (14,0) node[ground]{};
        \draw (14,2) to [open, v=$V_o$] (14,0);
    \end{circuitikz}
    
    \caption{Part II: Four-pole RC Filter}
    \label{fig:part2_circuit}
\end{figure}

\begin{table}[H]
    \begin{center}
        \begin{tabular}{c c c c c c c c}
        \hline
        $R_1$ & $R_2$ & $R_3$ & $R_4$ & $C_1$ & $C_2$ & $C_3$ & $C_4$\\
        \hline
        $50\Omega$ & $1k\Omega$ & $1k\Omega$ & $1k\Omega$ & $20\mu F$ & $100pF$ & $500nF$ & $100pF$\\
        \hline
        \end{tabular}
    \end{center}
    \caption{Part II: Component Values for Figure \ref{fig:part2_circuit}}
    \label{tabel:2}
\end{table}

We wish to approximate the poles of the filter by considering the circuit at low and high frequencies relative to the capacitors used. We begin by analyzing the low frequency capacitors, and treat $C_2$ and $C_4$ as open circuits. Starting with $C_1$, we find the resistance seen by shorting $C_3$ to be,

\[
R_{1s} = R_1 + R_2||(R_3+R_4)
\]

Similarly, the resistance seen by $C_3$ from shorting $C_1$ is found to be,
\[
R_{2s} = R_1||R_2 + R_3 + R_4
\]

Turning our attention to the high frequency capacitors we treat $C_1$ and $C_3$ as shorts. Making $C_4$ an open circuit, we find the resistance seen by $C_2$ to be,

\[
R_{1o} = (R_1||R_2)||(R_3+R_4)
\]

By making $C_2$ an open circuit, the resistance seen by $C_4$ is,

\[
R_{2o} = ((R_1||R_2)+R_3)||R_4
\]

The calculated resistance($R$), open/short circuit time constant($\tau$), and pole frequency ($\omega$) are summarized below in Table \ref{tab:part2_time_constants}.\\


\begin{table}[H]
    \centering
    \begin{tabular}{c | c}
        \hline 
        Short Circuit & Open Circuit \\
        \hline 
        $R_{1s} = 716.67\Omega$ & $R_{1o} = 46.51\Omega$ \\
        $R_{2s} = 2.047k\Omega$ & $R_{2o} = 511.63\Omega$ \\
        $\tau_{LP1} = 14.33\times10^{-2}$s & $\tau_{HP1} = 4.65\times10^{-9}$s \\
        $\tau_{LP2} = 1.02\times10^{-3}$s & $\tau_{HP2} = 5.12\times10^{-8}$s \\
        $\omega_{LP1} = 69.77$rad/s & $\omega_{HP1} = 2.15\times10^{8}$rad/s \\
        $\omega_{LP2} = 976.74$rad/s & $\omega_{HP2} = 1.95\times10^{7}$rad/s \\
        \hline
    \end{tabular}
    \caption{Part III: Calculated Poles, Time Constants, and Equivalent Resistances}
    \label{tab:part2_time_constants}
\end{table}

To find the midband gain, we consider $C_1$ and $C_3$ to be short circuits and $C_2$ and $C_4$ to be open circuits and solve for the relation between input and output voltage.

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_i$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_1$] (4,2);
        \draw (4,2) to [R=$R_2$] (4,0);
        \draw (5,2) to (4,2) node[label=$V_1$]{};
        \draw (4,0) to (4,0) node[ground]{};
        \draw (4,2) to [R=$R_3$] (8,2);
        \draw (8,2) to [R=$R_4$] (8,0);
        \draw (8,0) to (8,0) node[ground]{};
        \draw (8,2) to [short,-o] (10,2);
        \draw (10,0) to (10,0) node[ground]{};
        \draw (10,0) to [short,-o] (10,0);
        \draw (10,0) to [open,v<=$V_o$] (10,2);
    \end{circuitikz}
    
    \caption{Part II: RC Filter at Midband}
    \label{fig:part2_midband}
\end{figure}

We can solve for $A_M$ with the following,

\[
        A_M = \frac{V_o}{V_i} = \frac{V_o}{V_1} \times \frac{V_1}{V_i}
\]

Where,

\[
        \frac{V_o}{V_1}  = \frac{R_4}{R_3 + R_4}
\]

\[
        \frac{V_1}{V_i}  = \frac{R_2||(R_3+R_4)}{R_1+R_2||(R_3+R_3)}
\]

Giving,

\[
        A_M = 0.45612
\]

 
From these values, we approximate the transfer function for the filter to be,

\[
T(s) = 0.46512 \times \frac{s}{s+69.77} \times \frac{s}{s+976.744} \times \frac{2.15\times10^{8}}{s+2.15\times10^{8}} \times \frac{1.95\times10^{7}}{s+1.95\times10^{8}}
\] \\

The circuit was then modeled in Circuit Maker, as shown in \ref{fig:part2_CM_circuit}. Figures \ref{fig:part2_magnitude} and \ref{fig:part2_phase} show the magnitude and phase response of the given circuit, with $C_3 = 500nF$. These plots were used to graphically identify the location of each pole of the transfer function (for details please see Appendix). The poles determined are presented in Table \ref{tab:part2_sxfer_poles}.\\

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partII_figures/part_II_circuit.png}
        \caption{Part II: Filter Modeled in Circuit Maker}
        \label{fig:part2_CM_circuit}
\end{figure}



\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partII_figures/part2_circuit_magnitude.png}
        \caption{Part II: Circuit and Transfer Function Magnitude Bode Plot (X=frequency(HZ) Y=voltage(dB))}
        \label{fig:part2_magnitude}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partII_figures/part2_circuit_phase.png}
        \caption{Part II: Circuit and Transfer Function Phase Bode Plot (X=frequency(HZ) Y=voltage(deg))}
        \label{fig:part2_phase}
\end{figure}

\begin{table}[H]
    \centering
    \begin{tabular}{c c c c}
        \hline 
        $f_{LP1}$ & $f_{LP2}$ & $f_{HP1}$ & $f_{HP2}$ \\
        \hline 
        $6.63$Hz & $164.8$Hz & $3.81$MHz & $36.8$MHz \\
        \hline 
    \end{tabular}
    \caption{Part II: Graphically Determine Transfer Function Pole Locations}
    \label{tab:part2_sxfer_poles}
\end{table}


The low frequency 3-dB point can be approximated from the values in Table \ref{tab:part2_time_constants} with,

\begin{equation}
    \omega_{L3dB} \approx \sum_{n=1}^{2} \omega_{LPn}
    \label{eqn:unkown_poles}
\end{equation}\\


From \ref{eqn:unkown_poles}, the approximate value of the low 3-dB point is found to be $f_{L3dB} = 166.55$Hz. However, we can also note that $f_{LP1} < 10 \times f_{LP2}$ and conclude that $C_3$ will still be acting as an open circuit during our time constant calculation for $C_1$. We can then re-calculate the time constant for $C_1$ and conclude these to be approximations for the exact values of the low frequency poles. From this we can improve the approximation for $f_{L3dB}$ with the following equation,

\begin{equation}
    \omega_{L3dB} \approx \sqrt{\omega_{LP1}^2 + \omega_{LP2}^2}
    \label{eqn:known_poles}
\end{equation}\\


After increasing the values of $C_3$,  $f_{LP2}$ was no longer more than 10 times larger than $f_{LP1}$, so the low frequency 3-dB values for these circuits were calculated with \eqref{eqn:unkown_poles}, rather than \eqref{eqn:known_poles}. From the AC simulations, we can graphically identify the low frequency 3-dB point (for figures of these plots please see Appendix). The calculated low frequency 3-dB values, AC simulated low frequency 3-dB values, and percentage error in the calculated values are given in Table \ref{tab:low_3dB}.\\

\begin{table}[H]
    \centering
    \begin{tabular}{c c c c}
        \hline
        $C_3$ & Calculated $f_{L3dB}$ & Simulated $f_{L3dB}$ & Percentage Error\\
        \hline
        $500nF$ & $155.64$Hz & $154.6$Hz & $0.67$\%\\
        $1\mu F$ & $88.83$Hz & $79.60$Hz & $11.6$\%\\
        $2\mu F$ & $49.97$Hz & $42.20$Hz & $18.4$\%\\
        $5\mu F$ & $26.65$Hz & $21.54$Hz & $23.72$\%\\
        $10\mu F$ & $18.88$Hz & $15.30$Hz & $23.37$\%\\
        \hline
    \end{tabular}
    \caption{Part II: Summary of Calculated and Simulated Low Frequency 3-dB Values}
    \label{tab:low_3dB}
\end{table} 

It can be seen that as the value of $C_3$ approaches $C_1$, the error in the approximation increases. As well, given we are no longer using the approximation for the exact poles, we expect the error to increase. These results demonstrate the assumptions made for the method of open/short circuit time constants. As the values of the low frequency poles get closer to each other (as is happening when we increas $C_3$), the capacitors interact with each other complicating the response.

\pagebreak

\section{Part III}

Figure \ref{fig:part3_circuit} shows a transconductance amplifier, whose component values are given in Table \ref{tabel:3}. We wish to calculate the midband gain and locations of all poles. This will be done by employing the method of open/short circuit time constants, in conjunction with Miller's theorem.

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_1$] (2,2);
        \draw (2,2) to [C=$C_1$] (4,2);
        \draw (4,2) to [R=$R_2$, name=R2] (4,0);
        \draw (4,0) to (4,0) node[ground]{};
        \draw (3.5,0) to [open, v<=$V_1$] (3.5,2);
        \draw (4,2) to [short] (6,2);
        \draw (6,2) to [C=$C_2$] (6,0);
        \draw (6,0) to (6,0) node[ground]{};
        \draw (6,2) to [C=$C_3$] (8,2);
        \draw (8,2) to [cI, l=$GV_1$] (8,0);
        \draw (8,0) to (8,0) node[ground]{};
        \draw (8,2) to [short] (10,2);
        \draw (10,2) to [R=$R_3$] (10,0);
        \draw (10,0) to (10,0) node[ground]{};
        \draw (10,2) to [C=$C_4$] (12,2);
        \draw (12,2) to [R=$R_4$] (12,0);
        \draw (12,0) to (12,0) node[ground]{};
        \draw (12,2) to [short,-o] (14,2);
        \draw (14,0) to (14,0) node[ground]{};
        \draw (14,0) to [short,-o] (14,0);
        \draw (14,0) to [open,v<=$V_o$] (14,2);
    \end{circuitikz}
    
    \caption{Part III: Basic Transconductance Amplifier}
    \label{fig:part3_circuit}
\end{figure}

\begin{table}[H]
    \begin{center}
        \begin{tabular}{c c c c c c c c c}
        \hline 
        $R_1$ & $R_2$ & $R_3$ & $R_4$ & $C_1$ & $C_2$ & $C_3$ & $C_4$ & G\\
        \hline
        $50\Omega$ & $1k\Omega$ & $4k\Omega$ & $4k\Omega$ & $1\mu F$ & $10pF$ & $2pF$ & $1\mu F$ & 0.1\\
        \hline
        \end{tabular}
    \end{center}
    \caption{Part III: Component Values for Figure \ref{fig:part3_circuit}}
    \label{tabel:3}
\end{table}

We begin by recognizing that $C_1$ and $C_4$ are low frequency capacitors, and $C_2$ and $C_3$ are high frequency capacitors. Therefore, in the midband region, $C_1$ and $C_4$ are short circuits, while $C_2$ and $C_3$ are open circuits. If we apply a source transformation to the voltage controlled current source, we arrive at the circuit in figure \ref{fig:part3_midband}.

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_1$] (4,2);
        \draw (4,2) to [R=$R_2$] (4,0);
        \draw (3.5,2) to [open,v=$V_1$] (3.5,0);
        \draw (4,0) to (4,0) node[ground]{};
        
        \draw (6,2) to [V, l=$-GV_1(R_3||R_4)$] (6,0);
        \draw (6,0) to (6,0) node[ground]{};
        \draw (6,2) to [R=$R_3||R_4$,] (10,2);
        \draw (10,2) to [short,-o] (11,2);
        \draw (11,0) to (11,0) node[ground]{};
        \draw (11,0) to [short,-o] (11,0);
        \draw (11,0) to [open,v<=$V_o$] (11,2);
    \end{circuitikz}
    
    \caption{Part III: Amplifier Circuit in Midband Frequency Region}
    \label{fig:part3_midband}
\end{figure}

Solving for the midband gain to be,

\[
A_m = \frac{V_o}{V_s} = -G\left( \frac{R_2}{R_1 + R_2} \right) (R_3 || R_4) = -190.48
\] 

To calculate the low frequency poles we find the short circuit time constants, while keeping $C_2$ and $C_3$ open circuits. Shorting $C_4$, we find that the equivalent resistance seen by $C_1$ is,

\[
R_{1s} = R_1 + R_2
\]

By shorting $C_1$, we find the equivalent resistance seen by $C_4$ to be,

\[
R_{2s} = R_3 + R_4
\]

To calculate the high frequency poles we apply Miller's Theorem to $C_3$ (ignoring negligible terms) and get the resulting circuit shown in figure \ref{fig:part3_miller}, finding the open circuit time constants while keeping $C_1$ and $C_4$ short circuits. 

\begin{figure}[H]
    \hspace{-1cm}
    \centering
    \begin{circuitikz}[american,scale=0.8,/tikz/circuitikz/bipoles/length=1cm] % make components and scale smaller
        \draw (0,0) to [sV, v<=$V_s$] (0,2);
        \draw (0,0) to (0,0) node[ground]{};
        \draw (0,2) to [R=$R_1$] (3,2);
        \draw (3,2) to [R=$R_2$] (3,0);
        \draw (3,0) to (3,0) node[ground]{};
        \draw (3,2) to [short] (5,2);
        \draw (5,2) to [C=$(C_2+C_3G(R_3||R_4))$] (5,0);
        \draw (5,0) to (5,0) node[ground]{};
        \draw (2.5,2) to [open,v=$V_1$] (2.5,0);
        
        \draw (10,2) to [V, l=$-GV_1(R_3||R_4)$] (10,0);
        \draw (10,0) to (10,0) node[ground]{};
        \draw (10,2) to [R=$R_3||R_4$] (14,2);
        \draw (14,2) to [C=$C_3$] (14,0);
        \draw (14,0) to (14,0) node[ground]{};
        \draw (14,2) to [short,-o] (16,2);
        \draw (16,0) to (16,0) node[ground]{};
        \draw (16,0) to [short,-o] (16,0);
        \draw (16,0) to [open,v<=$V_o$] (16,2);
    \end{circuitikz}
    \caption{Part III: Amplifier Circuit High Frequency Region}
    \label{fig:part3_miller}
\end{figure}


Making $C_3$ an open circuit, the equivalent resistance seen by the input capacitance is,
\[
R_{1o} = R_1 || R_2
\]

Making the input capacitance an open circuit, the equivalent resistance seen by $C_3$ is,

\[
R_{2o} = R_3 || R_4
\] 

The pole, and supplementary, values calculated from the method of open/short circuit time constants are given in Table \ref{tab:part3_time_constants}.

\begin{table}[H]
    \centering
    \begin{tabular}{c | c}
        \hline 
        Short Circuit & Open Circuit \\
        \hline 
        $R_{1s} = 1050\Omega$ & $R_{1o} = 47.62\Omega$ \\
        $R_{2s} = 8000\Omega$ & $R_{2o} = 2000\Omega$ \\
        $\tau_{LP1} = 1.05\times10^{-3}$s & $\tau_{HP1} = 1.95\times10^{-8}$s \\
        $\tau_{LP2} = 8.0\times10^{-3}$s & $\tau_{HP2} = 4.0\times10^{-9}$s \\
        $\omega_{LP1} = 952.38$rad/s & $\omega_{HP1} = 5.12\times10^{7}$rad/s \\
        $\omega_{LP2} = 125$rad/s & $\omega_{HP2} = 2.5\times10^{8}$rad/s \\
        \hline
    \end{tabular}
    \caption{Part III: Calculated Poles, Time Constants, and Equivalent Resistances}
    \label{tab:part3_time_constants}
\end{table} 

The values in Table \ref{tab:part3_time_constants} are used to build the transfer function of the amplifier, given by,

\[
        T(s) = -190.5 \times \frac{s}{s+952.38} \times \frac{s}{s+125} \times \frac{5.12\times10^4}{s+5.12\times10^7} \times \frac{2.5\times10^8}{s+2.5\times10^8}
\] \\

The amplifier circuit was then modeled in Circuit Maker, as shown in \ref{fig:part3_CMcircuit}, to run AC simulations determining the magnitude and phase response of the circuit. These plots are given in \ref{fig:part3_mag} and \ref{fig:part3_phase}.

\begin{figure}[H]
    \centering
    \includegraphics[width=0.9\linewidth]{partIII_figures/part3_circuit.png}
    \caption{Part III: Amplifier Circuit Modeled in Circuit Maker}
    \label{fig:part3_CMcircuit}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partIII_figures/part3_magnitude.png}
        \caption{Part III: Circuit Magnitude Bode Plot (X=frequency(HZ) Y=voltage(dB))}
        \label{fig:part3_mag}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{partIII_figures/part3_phase.png}
        \caption{Part III: Circuit Phase Bode Plot (X=frequency(HZ) Y=voltage(deg))}
        \label{fig:part3_phase}
\end{figure}

The values for the poles of the transfer function were graphically determined as in Part II; Table \ref{tab:part3_poles_compared} compares these determined values from the AC simulation to those calculated above. Table \ref{tab:part3_3dB} show the calculated high and low 3-dB points, as compared the the results of the AC simulation. Given the proximity of the calculated poles, the low 3-dB point was calculated using \eqref{eqn:unkown_poles}, while the high 3-dB point was calculated using,

\begin{equation}
    \omega_{H3dB} \approx \frac{1}{\sum\limits_{i=1}^2 \tau_{HPi}\omega_{HPi}}
\end{equation}

\begin{table}[H]
    \centering
    \begin{tabular}{c c}
        \hline 
        Graphically Determined $f$ & Calculated $f$\\
        \hline
        $19.70$Hz & $19.89$Hz \\
        $148.10$Hz & $151.58$Hz \\
        $10.18$MHz & $8.15$MHz \\
        $41.48$MHz & $39.8$MHz \\
        \hline
    \end{tabular}
    \caption{Part III: Comparison of Calculated Poles to Graphical Poles}
    \label{tab:part3_poles_compared}
\end{table} 

\begin{table}[H]
    \centering
    \begin{tabular}{c c c | c c c}
        \hline 
        \multicolumn{3}{c|}{$f_{L3dB}$} & \multicolumn{3}{c}{$f_{H3dB}$} \\
        Simulated & Calculated & \% Error & Simulated & Calculated & \% Error \\
        \hline 
        $153.3$Hz & $171.47$Hz & $11.85$ & $6.52$MHz & $6.77$MHz & $3.83$ \\ 
        \hline
    \end{tabular}
    \caption{Part III: Comparison Simulated and Calculated 3-dB Points}
    \label{tab:part3_3dB}
\end{table} 

It can be seen that the calculated values agree relatively well with the simulated values. Noticably, the error is lower for the high 3-dB point than the low. Again, we consider the distance between the high pole frequencies compared to the distance of the low poles. Although in neither case is the higher value greater than $10$ times the smaller, the high frequency poles are much farther apart in frequency. Therefore we expect less interaction between the capacitors leading to better approximations.

\pagebreak

\section{Appendix}

\subsection{Graphical Determination of Poles}
The location of poles resulting from the AC simulations were determined by an effort to approximate the changes in slope of the magnitude Bode Plots for parts II and III. The plots resulting from simulations were brought into Photoshop to determine these points. The figures were then re-created with markers in these approximate locations to determine the frequencies. Figures \ref{fig:part2_graphical} and \ref{fig:part3_graphical} show such for parts II and III respectively. 

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{appendix/part2_estimate.png}
        \caption{Part II: Graphical Determination of Poles}
        \label{fig:part2_graphical}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{appendix/part3_poles.png}
        \caption{Part III: Graphical Determination of Poles}
        \label{fig:part3_graphical}
\end{figure}

\subsection{Graphical Determination of 3-dB Points}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{appendix/part2_3dB.png}
        \caption{Part II: Graphical Determination of 3-dB Points}
        \label{fig:part2_3dB}
\end{figure}

\begin{figure}[H]
    \centering
        \includegraphics[width=0.8\linewidth]{appendix/part3_3dB.png}
        \caption{Part III: Graphical Determination of 3-dB Points}
        \label{fig:part3_3dB}
\end{figure}

\subsection{Percentage Error Calculation}

The following equation was used to calculate the percentage error of points,

\[
\delta = \frac{|V_{expected} - V_{actual}|}{V_{actual}} \times 100\%
\]

\end{document}

